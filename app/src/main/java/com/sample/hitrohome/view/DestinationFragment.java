package com.sample.hitrohome.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.sample.hitrohome.R;
import com.sample.hitrohome.interfaces.AddressResponseListener;
import com.sample.hitrohome.interfaces.OnSelectedDestinationListener;


public class DestinationFragment extends Fragment implements AddressResponseListener {

    public String pickupAddress;
    private TextView txtPickupAddress;
    private TextView txtDestinationAddress;
    private ImageButton btnBack;
    private Button btnSetDestination;

    private OnSelectedDestinationListener onSelectedDestinationListener;

    public DestinationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_destination, container, false);

        txtPickupAddress = view.findViewById(R.id.txtPickupAddress);

        txtDestinationAddress = view.findViewById(R.id.txtDestinationAddress);

        btnBack = view.findViewById(R.id.btnBack);

        btnSetDestination = view.findViewById(R.id.btnSetDestination);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSetDestination.setEnabled(false);

        btnSetDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectedDestinationListener.onSelectedDestination();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity()!=null)
                    getActivity().onBackPressed();
            }
        });

        txtPickupAddress.setText(pickupAddress);
    }

    @Override
    public void onError() {
        if(isAdded()){
            btnSetDestination.setEnabled(false);
            txtDestinationAddress.setText(getString(R.string.internet_access_error));
        }

    }

    @Override
    public void onSuccess(String message) {
        if(isAdded()){
            txtDestinationAddress.setText(message);
            btnSetDestination.setEnabled(true);
        }

    }

    @Override
    public void onLoading() {
        if(isAdded()){
            btnSetDestination.setEnabled(false);
            txtDestinationAddress.setText(getString(R.string.loading_address));
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof OnSelectedDestinationListener){
            onSelectedDestinationListener = (OnSelectedDestinationListener) context;
        }else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSelectedDestinationListener");
        }
    }

    @Override
    public void onDetach() {
        onSelectedDestinationListener = null;
        super.onDetach();
    }
}
