package com.sample.hitrohome.view.Utils;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.sample.hitrohome.interfaces.OnMapMotionEventListener;

public class TouchableWrapper extends FrameLayout {

    private OnMapMotionEventListener mMotionEvent;

    public TouchableWrapper(Context context) {
        super(context);
    }

    public void setOnMotionEventListener(OnMapMotionEventListener motionEvent){
        this.mMotionEvent = motionEvent;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if(mMotionEvent != null)
                    mMotionEvent.actionDown();
                break;

            case MotionEvent.ACTION_UP:
                if(mMotionEvent != null)
                    mMotionEvent.actionUp();
                break;
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    protected void onDetachedFromWindow() {
        mMotionEvent = null;
        super.onDetachedFromWindow();
    }
}
