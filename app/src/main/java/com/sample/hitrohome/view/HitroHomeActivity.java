package com.sample.hitrohome.view;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sample.hitrohome.Model.ReversAddressModel;
import com.sample.hitrohome.R;
import com.sample.hitrohome.ViewModel.HitroHomeActivityViewModel;
import com.sample.hitrohome.interfaces.OnMapMotionEventListener;
import com.sample.hitrohome.interfaces.OnSelectedDestinationListener;
import com.sample.hitrohome.interfaces.OnSelectedPickupListener;
import com.sample.hitrohome.view.Utils.MySupportMapFragment;
import com.sample.hitrohome.view.Utils.ViewUtils;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class HitroHomeActivity extends FragmentActivity implements
        OnMapReadyCallback
        ,OnMapMotionEventListener
        ,OnSelectedPickupListener
        ,OnSelectedDestinationListener
        ,Observer<ReversAddressModel> {

    private GoogleMap mMap;

    private HitroHomeActivityViewModel viewModel;

    private CompositeDisposable compositeDisposable;

    private SupportMapFragment mapFragment;

    private PickupFragment pickupFragment;

    private DestinationFragment destinationFragment;

    private ImageView imgPin;

    private final int PICKUP_FRAGMENT_NUMBER = 1;

    private final int DESTINATION_FRAGMENT_NUMBER = 2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgPin = findViewById(R.id.imgPin);

        mapFragment = (MySupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        compositeDisposable = new CompositeDisposable();

        viewModel = ViewModelProviders.of(this).get(HitroHomeActivityViewModel.class);

        commitPickupFragment();

    }


    @Override
    public void onBackPressed() {

        int countOfFragments = getSupportFragmentManager().getBackStackEntryCount();

        switch (countOfFragments){

            case PICKUP_FRAGMENT_NUMBER:{

                finish();
                break;
            }

            case DESTINATION_FRAGMENT_NUMBER:{

                getSupportFragmentManager().popBackStack();
                cancelRequest();

                break;
            }

            default:{
                finish();
            }

        }
    }

    private void cancelRequest() {

        imgPin.setImageResource(R.drawable.ic_pin_black);

        destinationFragment = null;

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(viewModel.getPickupAddressLatLon(), viewModel.ADD_MARKER_ZOOM), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                if(mMap != null)
                    getAddress();

            }

            @Override
            public void onCancel() {
                if(mMap != null)
                    getAddress();
            }
        });

        mMap.clear();
    }

    private void getAddress() {
        compositeDisposable.clear();
        viewModel.getAddress(Double.toString(mMap.getCameraPosition().target.latitude), Double.toString(mMap.getCameraPosition().target.longitude))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this);
    }


    private void commitPickupFragment() {
        pickupFragment = new PickupFragment();

        commitFragment(pickupFragment);
    }

    private void commitDestinationFragment() {

        destinationFragment = new DestinationFragment();

        commitFragment(destinationFragment);

        destinationFragment.pickupAddress = viewModel.getPickupDisplayName();
    }

    private void commitFragment(Fragment fragment){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame,fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng tehranLatLon = viewModel.getTehranLatLon();

        final float mapZoom = viewModel.DEFAULT_MAP_ZOOM;

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(tehranLatLon, mapZoom), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                if(mMap!=null)
                    getAddress();
            }

            @Override
            public void onCancel() {
                    if(mMap!=null)
                        getAddress();
            }
        });

        ((MySupportMapFragment) mapFragment).setMotionEvent(this);
    }


    @Override
    protected void onDestroy() {

        if (!compositeDisposable.isDisposed()) compositeDisposable.dispose();

        super.onDestroy();
    }

    //RX
    @Override
    public void onSubscribe(Disposable d) {
        compositeDisposable.add(d);
    }

    //RX
    @Override
    public void onNext(ReversAddressModel addressModel) {
        if(pickupFragment!=null) pickupFragment.onSuccess(addressModel.getDisplay_name());
        if(destinationFragment!=null) destinationFragment.onSuccess(addressModel.getDisplay_name());
    }

    //RX
    @Override
    public void onError(Throwable e) {
        if(pickupFragment!=null) pickupFragment.onError();
        if(destinationFragment!=null) destinationFragment.onError();
    }

    //RX
    @Override
    public void onComplete() {
    }


    //Map Motion events listener
    @Override
    public void actionDown() {
        if(pickupFragment!=null) pickupFragment.onLoading();
        if(destinationFragment!=null) destinationFragment.onLoading();

    }

    //Map Motion events listener
    @Override
    public void actionUp() {
        getAddress();
    }


    //Pickup Fragment
    @Override
    public void onSelectedPickup() {

        final double lat = mMap.getCameraPosition().target.latitude;
        final double lon = mMap.getCameraPosition().target.longitude;

        LatLng latLngPickup = new LatLng(lat, lon);

        viewModel.pickupSelected();

        mMap.addMarker(new MarkerOptions().position(latLngPickup).icon(ViewUtils.bitmapDescriptorFromVector(this, R.drawable.ic_pin_black)));

        imgPin.setImageResource(R.drawable.ic_pin_orange);

        commitDestinationFragment();

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat - viewModel.RELATIVE_DISTANCE_ON_THE_MAP, lon + viewModel.RELATIVE_DISTANCE_ON_THE_MAP), viewModel.ADD_MARKER_ZOOM), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                if(mMap!=null) getAddress();
            }

            @Override
            public void onCancel() {
                if(mMap!=null) getAddress();
            }
        });

    }

    //Destination Fragment
    @Override
    public void onSelectedDestination() {
        viewModel.destinationSelected();
        Toast.makeText(this,"onSelectedDestination",Toast.LENGTH_SHORT).show();
    }

}
