package com.sample.hitrohome.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.sample.hitrohome.R;
import com.sample.hitrohome.interfaces.AddressResponseListener;
import com.sample.hitrohome.interfaces.OnSelectedPickupListener;

public class PickupFragment extends Fragment implements AddressResponseListener {

    private OnSelectedPickupListener onSelectedPickupListener;

    private Button btnPickup;

    private TextView txtAddress;


    public PickupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_pickup, container, false);

        btnPickup = view.findViewById(R.id.btnPickup);
        txtAddress = view.findViewById(R.id.txtAddress);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnPickup.setEnabled(false);
        btnPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onSelectedPickupListener.onSelectedPickup();
            }
        });
    }

    @Override
    public void onError() {
        btnPickup.setEnabled(false);
        txtAddress.setText(getString(R.string.internet_access_error));
    }

    @Override
    public void onSuccess(String message) {
        txtAddress.setText(message);
        btnPickup.setEnabled(true);
    }

    @Override
    public void onLoading() {
        btnPickup.setEnabled(false);
        txtAddress.setText(getString(R.string.loading_address));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof OnSelectedPickupListener){
            onSelectedPickupListener = (OnSelectedPickupListener) context;
        }else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSelectedPickupListener");
        }
    }


    @Override
    public void onDetach() {
        onSelectedPickupListener = null;
        super.onDetach();

    }
}
