package com.sample.hitrohome.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.sample.hitrohome.Model.ReversAddressModel;
import com.sample.hitrohome.repository.HitroRepository;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class HitroHomeActivityViewModel extends AndroidViewModel {

    private HitroRepository hitroRepository;



    public final float DEFAULT_MAP_ZOOM = 10.0f;

    public final float ADD_MARKER_ZOOM = 15.0f;

    public final float RELATIVE_DISTANCE_ON_THE_MAP = 0.002f;

    private ReversAddressModel currentReverseAddressModel;

    private ReversAddressModel pickupReverseAddressModel;

    private ReversAddressModel destinationReverseAddressModel;

    private CompositeDisposable compositeDisposable;



    public HitroHomeActivityViewModel(@NonNull Application application) {
        super(application);

        hitroRepository = new HitroRepository();

        compositeDisposable = new CompositeDisposable();
    }

    public Observable<ReversAddressModel> getAddress(final String lat, final String lon){

        compositeDisposable.clear();

        return  Observable.create(new ObservableOnSubscribe<ReversAddressModel>() {
            @Override
            public void subscribe(final ObservableEmitter<ReversAddressModel> emitter) throws Exception {
                hitroRepository.reverseAddress(lat,lon).subscribe(new Observer<ReversAddressModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onNext(ReversAddressModel reversAddressModel) {

                        currentReverseAddressModel = reversAddressModel;

                        if(!emitter.isDisposed()) emitter.onNext(reversAddressModel);
                    }

                    @Override
                    public void onError(Throwable e) { if(!emitter.isDisposed()) emitter.onError(e); }

                    @Override
                    public void onComplete() {
                        if(!emitter.isDisposed()) emitter.onComplete();
                    }
                });

            }
        });
    }

    public LatLng getTehranLatLon() {
       return new LatLng(35.7034962, 51.389216);
    }


    public void pickupSelected() {
        this.pickupReverseAddressModel = currentReverseAddressModel;
    }

    public void destinationSelected(){this.destinationReverseAddressModel = currentReverseAddressModel;}

    public void cancelRequest(){
        destinationReverseAddressModel = null;
        pickupReverseAddressModel = null;
    }

    public String getPickupDisplayName(){
      return pickupReverseAddressModel.getDisplay_name();
    }

    public LatLng getPickupAddressLatLon() {

        if(pickupReverseAddressModel != null){
            String lat = pickupReverseAddressModel.getLat();
            String lon = pickupReverseAddressModel.getLon();

            return  new LatLng(Double.valueOf(lat),Double.valueOf(lon));
        }else{
            return  getTehranLatLon();
        }
    }

    @Override
    protected void onCleared() {
        if(!compositeDisposable.isDisposed()) compositeDisposable.dispose();
        super.onCleared();
    }
}
