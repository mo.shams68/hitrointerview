package com.sample.hitrohome.repository;


import com.sample.hitrohome.Model.ReversAddressModel;
import com.sample.hitrohome.repository.retrofit.RetrofitClient;
import com.sample.hitrohome.repository.retrofit.RetrofitConst;
import io.reactivex.Observable;


public class HitroRepository {

    private RetrofitClient retrofitClient;

    public HitroRepository(){
        retrofitClient = RetrofitClient.getInstance();

    }

    public Observable<ReversAddressModel> reverseAddress(String lat, String lon){
        return retrofitClient.APIs().reverseAddress(RetrofitConst.JSON_V_2, lat, lon);
    }

}
