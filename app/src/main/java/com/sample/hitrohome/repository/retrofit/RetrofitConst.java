package com.sample.hitrohome.repository.retrofit;

public class RetrofitConst {

    public static final String BASE_URL = "https://nominatim.openstreetmap.org/";
    public static final String REVERSE = "reverse";
    public static final String JSON_V_2 = "jsonv2";
}
