package com.sample.hitrohome.repository.retrofit;

import com.sample.hitrohome.Model.ReversAddressModel;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {
    @GET(RetrofitConst.REVERSE)
    Observable<ReversAddressModel> reverseAddress(
             @Query("format") String format
            ,@Query("lat") String lat
            ,@Query("lon") String lon);
}
