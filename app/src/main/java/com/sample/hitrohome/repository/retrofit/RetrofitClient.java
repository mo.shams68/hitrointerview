package com.sample.hitrohome.repository.retrofit;


import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static RetrofitClient instance = null;

    private APIService apiServices;


    private RetrofitClient(){

        Retrofit retrofit = new Retrofit.Builder().baseUrl(RetrofitConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getOkHttpClient())
                .build();


        apiServices = retrofit.create(APIService.class);
    }


    public static RetrofitClient getInstance(){
        if(instance == null){
            instance = new RetrofitClient();
        }

        return  instance;
    }

    private OkHttpClient getOkHttpClient(){
        return  new OkHttpClient
                .Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    public APIService APIs(){
        return apiServices;
    }


}
