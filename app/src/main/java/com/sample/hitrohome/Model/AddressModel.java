package com.sample.hitrohome.Model;

import com.google.gson.annotations.SerializedName;

public class AddressModel {

    @SerializedName("neighbourhood")
    private String neighbourhood;

    @SerializedName("suburb")
    private String suburb;

    @SerializedName("road")
    private String road;

    @SerializedName("city")
    private String city;

    @SerializedName("county")
    private String county;

    @SerializedName("state")
    private String state;


    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

