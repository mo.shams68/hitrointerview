package com.sample.hitrohome.interfaces;


public interface OnMapMotionEventListener {
    void actionDown();
    void actionUp();
}
