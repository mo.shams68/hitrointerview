package com.sample.hitrohome.interfaces;

public interface AddressResponseListener {
    void onError();
    void onSuccess(String message);
    void onLoading();
}
