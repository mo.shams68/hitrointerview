package com.sample.hitrohome.interfaces;

public interface OnSelectedDestinationListener {
    void onSelectedDestination();
}
